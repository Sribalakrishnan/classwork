public class TotalBillAmount{
	public static void main (String arg[]){
		int unit=20;
		System.out.println("Total Unit - "+unit);
		if (unit>=0){
			if (unit<=30){
				int cost=(unit*3);
				System.out.println("Total Cost - "+cost);
			}else if ((unit>30)&&(unit<=60)){
				int cost=(30*3)+(unit-30)*5;
				System.out.println("Total Cost - "+cost);
			}else if ((unit>60)&&(unit<=90)){
				int cost=(30*3)+(30*5)+(unit-60)*10;
				System.out.println("Total Cost - "+cost);
			}else if ((unit>90)&&(unit<=120)){
				int cost=(30*3)+(30*5)+(30*10)+(unit-90)*20;
				System.out.println("Total Cost - "+cost);
			}else if ((unit>120)&&(unit<=180)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(unit-120)*30;
				System.out.println("Total Cost - "+cost);
			}else if ((unit>180)&&(unit<=210)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(unit-180)*35;
				System.out.println("Total Cost - "+cost);
			}else if ((unit>210)&&(unit<=300)){
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(unit-210)*40;
				System.out.println("Total Cost - "+cost);
			}else{
				int cost=(30*3)+(30*5)+(30*10)+(30*20)+(60*30)+(30*35)+(90*50)+(unit-300)*50;
				System.out.println("Total Cost - "+cost);
			}
		}
	}
}
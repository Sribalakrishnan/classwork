public class SwitchcaseDemo{
	public static void main(String[]args){
		int num=3;
		switch(num){
		case 1:
			System.out.println("case1 :value is:" + num);
			break;
		case 2:
			System.out.println("case2 :value is:" + num);
			break;
		case 3:
			System.out.println("case3 :value is:" + num);
			break;
		default:
			System.out.println("Default :value is:" + num);
		}
	}
}